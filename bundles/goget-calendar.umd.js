(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('goget-calendar', ['exports', '@angular/core'], factory) :
    (factory((global['goget-calendar'] = {}),global.ng.core));
}(this, (function (exports,i0) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/goget-calendar.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var GogetCalendarService = /** @class */ (function () {
        function GogetCalendarService() {
        }
        GogetCalendarService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        GogetCalendarService.ctorParameters = function () { return []; };
        /** @nocollapse */ GogetCalendarService.ngInjectableDef = i0.defineInjectable({ factory: function GogetCalendarService_Factory() { return new GogetCalendarService(); }, token: GogetCalendarService, providedIn: "root" });
        return GogetCalendarService;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/goget-calendar.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var GogetCalendarComponent = /** @class */ (function () {
        function GogetCalendarComponent() {
        }
        /**
         * @return {?}
         */
        GogetCalendarComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        GogetCalendarComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'goget-goget-calendar',
                        template: "\n    <p>\n      goget-calendar works!\n    </p>\n  "
                    }] }
        ];
        /** @nocollapse */
        GogetCalendarComponent.ctorParameters = function () { return []; };
        return GogetCalendarComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/calendar/calendar.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CalendarComponent = /** @class */ (function () {
        function CalendarComponent() {
        }
        /**
         * @return {?}
         */
        CalendarComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        CalendarComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'goget-calendar',
                        template: "<p>\r\n  calendar works!\r\n</p>\r\n",
                        styles: [""]
                    }] }
        ];
        /** @nocollapse */
        CalendarComponent.ctorParameters = function () { return []; };
        return CalendarComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/goget-calendar.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var GogetCalendarModule = /** @class */ (function () {
        function GogetCalendarModule() {
        }
        GogetCalendarModule.decorators = [
            { type: i0.NgModule, args: [{
                        declarations: [GogetCalendarComponent, CalendarComponent],
                        imports: [],
                        exports: [GogetCalendarComponent]
                    },] }
        ];
        return GogetCalendarModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: public-api.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * Generated from: goget-calendar.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.GogetCalendarService = GogetCalendarService;
    exports.GogetCalendarComponent = GogetCalendarComponent;
    exports.GogetCalendarModule = GogetCalendarModule;
    exports.ɵa = CalendarComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=goget-calendar.umd.js.map