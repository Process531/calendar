/**
 * @fileoverview added by tsickle
 * Generated from: lib/goget-calendar.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var GogetCalendarComponent = /** @class */ (function () {
    function GogetCalendarComponent() {
    }
    /**
     * @return {?}
     */
    GogetCalendarComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    GogetCalendarComponent.decorators = [
        { type: Component, args: [{
                    selector: 'goget-goget-calendar',
                    template: "\n    <p>\n      goget-calendar works!\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    GogetCalendarComponent.ctorParameters = function () { return []; };
    return GogetCalendarComponent;
}());
export { GogetCalendarComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ29nZXQtY2FsZW5kYXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZ29nZXQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvZ29nZXQtY2FsZW5kYXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUVsRDtJQVdFO0lBQWdCLENBQUM7Ozs7SUFFakIseUNBQVE7OztJQUFSO0lBQ0EsQ0FBQzs7Z0JBZEYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxzQkFBc0I7b0JBQ2hDLFFBQVEsRUFBRSxzREFJVDtpQkFFRjs7OztJQVFELDZCQUFDO0NBQUEsQUFoQkQsSUFnQkM7U0FQWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdnb2dldC1nb2dldC1jYWxlbmRhcicsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxwPlxyXG4gICAgICBnb2dldC1jYWxlbmRhciB3b3JrcyFcclxuICAgIDwvcD5cclxuICBgLFxyXG4gIHN0eWxlczogW11cclxufSlcclxuZXhwb3J0IGNsYXNzIEdvZ2V0Q2FsZW5kYXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==