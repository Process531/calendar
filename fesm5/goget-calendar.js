import { Injectable, Component, NgModule, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/goget-calendar.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var GogetCalendarService = /** @class */ (function () {
    function GogetCalendarService() {
    }
    GogetCalendarService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    GogetCalendarService.ctorParameters = function () { return []; };
    /** @nocollapse */ GogetCalendarService.ngInjectableDef = defineInjectable({ factory: function GogetCalendarService_Factory() { return new GogetCalendarService(); }, token: GogetCalendarService, providedIn: "root" });
    return GogetCalendarService;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/goget-calendar.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var GogetCalendarComponent = /** @class */ (function () {
    function GogetCalendarComponent() {
    }
    /**
     * @return {?}
     */
    GogetCalendarComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    GogetCalendarComponent.decorators = [
        { type: Component, args: [{
                    selector: 'goget-goget-calendar',
                    template: "\n    <p>\n      goget-calendar works!\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    GogetCalendarComponent.ctorParameters = function () { return []; };
    return GogetCalendarComponent;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/calendar/calendar.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var CalendarComponent = /** @class */ (function () {
    function CalendarComponent() {
    }
    /**
     * @return {?}
     */
    CalendarComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    CalendarComponent.decorators = [
        { type: Component, args: [{
                    selector: 'goget-calendar',
                    template: "<p>\r\n  calendar works!\r\n</p>\r\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    CalendarComponent.ctorParameters = function () { return []; };
    return CalendarComponent;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/goget-calendar.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var GogetCalendarModule = /** @class */ (function () {
    function GogetCalendarModule() {
    }
    GogetCalendarModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [GogetCalendarComponent, CalendarComponent],
                    imports: [],
                    exports: [GogetCalendarComponent]
                },] }
    ];
    return GogetCalendarModule;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: goget-calendar.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { GogetCalendarService, GogetCalendarComponent, GogetCalendarModule, CalendarComponent as ɵa };

//# sourceMappingURL=goget-calendar.js.map